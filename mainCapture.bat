@echo off

rem first parameter, save folder location
set savedir=%1
rem second parameter, model name
set model=%2

rem 300 seconds timeout
set /a looptime= 300 * 1000
set /p looptime=<looptime.txt

:loop

rem note "MM" MUST BE CAPITALIZED!
for /f %%a in ('powershell -command "get-date -format yyyyMMdd-hhmmss"') do set datetime=%%a

cls
echo ################################
echo %model% %datetime% %savedir%
echo ################################
echo.

set file=%savedir%\%model%-%datetime%.flv

call :rtmp

call :setsize %file%

if %size%==0 del %file%

ping 192.0.2.1 -n 1 -w %looptime% > nul

goto loop

:rtmp

set /a ser1=%random% %%50 + 5
set /a ser2=%random% %%13 + 2
set /a ser3=%random% %%2 + 1

if %ser3% equ 1 (set ser3=a) else (set ser3=b)

rem echo using rtmpdump with:
rem echo "rtmp://edge%ser1%-%ser3%.stream.highwebmedia.com/live-edge"
rem echo "mp4:rtmp://origin%ser2%.stream.highwebmedia.com/live-origin/%model%"

rem NOTE: RTMPDUMP ARGS ARE CASE SENSETIVE!
rem with --quiet
rem rtmpdump --quiet -r "rtmp://edge%ser1%-%ser3%.stream.highwebmedia.com/live-edge" -a "live-edge" -f "win 20,0,0,267" -w "https://chaturbate.com/static/flash/cbv_2p647.swf" -p "https://chaturbate.com/%model%/" -c s:anonymoususer -c s:%model% -c s:2.646 -c s:anonymous -c s:8f034c5320f2d277c18d2c60da29eb8d2201d933ad843e38d87df9dc3ff26150 --live -y "mp4:rtmp://origin%ser2%.stream.highwebmedia.com/live-origin/%model%" -o "%file%"
rem without --quiet
rtmpdump -r "rtmp://edge%ser1%-%ser3%.stream.highwebmedia.com/live-edge" -a "live-edge" -f "WIN 20,0,0,267" -W "https://chaturbate.com/static/flash/CBV_2p647.swf" -p "https://chaturbate.com/%model%/" -C S:AnonymousUser -C S:%model% -C S:2.646 -C S:anonymous -C S:8f034c5320f2d277c18d2c60da29eb8d2201d933ad843e38d87df9dc3ff26150 --live -y "mp4:rtmp://origin%ser2%.stream.highwebmedia.com/live-origin/%model%" -o "%file%"

goto :eof

:setsize
set size=%~z1
goto :eof